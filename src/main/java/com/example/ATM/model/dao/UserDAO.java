package com.example.ATM.model.dao;

import com.example.ATM.model.entity.User;
import com.example.ATM.controller.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {

    private UserRepository userRepository;

    @Autowired
    public UserDAO(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(User theUser) {
        userRepository.save(theUser);
    }
}
