package com.example.ATM;

import com.example.ATM.controller.service.TransactionService;
import com.example.ATM.model.dao.UserDAO;
import com.example.ATM.controller.service.UserService;
import com.example.ATM.view.LoginForm;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.awt.*;

@SpringBootApplication
public class AtmApplication {

	public static void main(String[] args) {
		System.setProperty("java.awt.headless", "false");
		SpringApplication.run(AtmApplication.class, args);
	}


	@Bean
	public CommandLineRunner commandLineRunner(UserService userService, TransactionService transactionService, UserDAO userDAO) {
		return args -> {
			EventQueue.invokeLater(() -> {
				showLoginForm(userService, transactionService, userDAO);
			});
		};
	}

	private void showLoginForm(UserService userService, TransactionService transactionService, UserDAO userDAO) {
		System.out.println("Initializing GUI...");
		LoginForm loginForm = new LoginForm(transactionService, userService, userDAO);
		loginForm.setLocationRelativeTo(null);
		loginForm.pack();
		loginForm.setVisible(true);
	}

}
