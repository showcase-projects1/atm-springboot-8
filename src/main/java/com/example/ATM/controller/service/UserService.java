package com.example.ATM.controller.service;


import com.example.ATM.controller.repository.UserRepository;
import com.example.ATM.controller.service.UserExistenceChecker;
import com.example.ATM.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;
    private UserExistenceChecker userExistenceChecker;

    @Autowired
    public UserService(UserRepository userRepository, UserExistenceChecker userExistenceChecker) {
        this.userRepository = userRepository;
        this.userExistenceChecker = userExistenceChecker;
    }

    public boolean doesUserExist(int userId) {
        return userExistenceChecker.doesUserExist(userId);
    }

    public Optional<User> authenticateUser(String username, String password) {
        User user = userRepository.findByUsername(username);
        if (user != null && user.getPassword().equals(password)) {
            return Optional.of(user);
        }
        return Optional.empty();
    }

    public Optional<User> getUserById(int userId) {
        return Optional.ofNullable(userRepository.findById(userId));
    }
}

