package com.example.ATM.controller.service;

import com.example.ATM.controller.repository.UserRepository;
import com.example.ATM.model.entity.User;
import com.example.ATM.other.exception.InsufficientBalanceException;
import com.example.ATM.other.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class TransactionService {

    private UserRepository userRepository;

    public TransactionService transferService() {
        return new TransactionService(userRepository);
    }

    @Autowired
    public TransactionService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public boolean transferFunds(int senderUserId, int recipientUserId, double amount) throws InsufficientBalanceException, UserNotFoundException {
        Optional<User> optionalSender = Optional.ofNullable(userRepository.findById(senderUserId));
        Optional<User> optionalRecipient = Optional.ofNullable(userRepository.findById(recipientUserId));

        User sender = optionalSender.orElseThrow(() -> new UserNotFoundException("User not found with ID: " + senderUserId));
        User recipient = optionalRecipient.orElseThrow(() -> new UserNotFoundException("User not found with ID: " + recipientUserId));

        if (sender.getBalance() < amount) {
            throw new InsufficientBalanceException("Insufficient balance");
        }
        try {
            sender.setBalance(sender.getBalance() - amount);
            recipient.setBalance(recipient.getBalance() + amount);

            userRepository.save(sender);
            userRepository.save(recipient);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}

