package com.example.ATM.view;

import com.example.ATM.model.dao.UserDAO;
import com.example.ATM.utility.CharacterLengthValidator;
import com.example.ATM.utility.MaskFormatterHelper;
import com.example.ATM.controller.service.TransactionService;
import com.example.ATM.controller.service.UserService;
import com.example.ATM.model.entity.User;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Optional;
import javax.swing.text.MaskFormatter;


public class LoginForm extends JFrame {
    private UserService userService;
    private TransactionService transactionService;
    private UserDAO userDAO;
    private JButton loginButton;
    private JButton createAccountButton;
    private JPanel panel;
    private JTextField usernameField;
    private JFormattedTextField passwordField;

    public LoginForm(TransactionService transactionService, UserService userService, UserDAO userDAO) {
        this.transactionService = transactionService;
        this.userService = userService;
        this.userDAO = userDAO;


        loginButton = new JButton("Login");
        createAccountButton = new JButton("Create new account");
        panel = new JPanel(new GridLayout(3, 1, 5, 0));
        usernameField = new JTextField(12);
        Optional<MaskFormatter> passwordFormatter = MaskFormatterHelper.createPasswordFormatter();
        passwordField = new JFormattedTextField(passwordFormatter.orElse(null));

        initComponents();
        addComponentsToPanel();
        addListeners();
        configureFrame();



        new CharacterLengthValidator(loginButton, usernameField, 5, 16);

    }

    private void initComponents() {
        loginButton.setBounds(100, 110, 90, 25);
        loginButton.setForeground(Color.WHITE);
        loginButton.setBackground(Color.DARK_GRAY);
        loginButton.setEnabled(false);

        createAccountButton.setBounds(100, 110, 90, 25);
        createAccountButton.setForeground(Color.WHITE);
        createAccountButton.setBackground(Color.DARK_GRAY);
    }

    private void addComponentsToPanel() {
        panel.add(new JLabel("Username"));
        panel.add(usernameField);
        panel.add(new JLabel("PIN Number (6 digits)"));
        panel.add(passwordField);
        panel.add(loginButton);
        panel.add(createAccountButton);
    }

    private void addListeners() {
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = usernameField.getText();
                String password = passwordField.getText();

                Optional<User> user = userService.authenticateUser(username, password);

                if (user.isPresent()) {
                    dispose();
                    WelcomePage welcomePage = new WelcomePage(user.get(), transactionService, userService, userDAO);
                    welcomePage.setLocationRelativeTo(null);
                    welcomePage.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Incorrect username or password", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        createAccountButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NewAccPage createAccountPage = new NewAccPage(transactionService,userService ,userDAO);
                createAccountPage.setLocationRelativeTo(null);
                createAccountPage.setVisible(true);
            }
        });

        usernameField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (loginButton.isEnabled()) {
                    loginButton.doClick();
                }
            }
        });

        passwordField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (loginButton.isEnabled()) {
                    loginButton.doClick();
                }
            }
        });
    }

    private void configureFrame() {
        add(panel, BorderLayout.CENTER);
        setSize(700, 300);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Login Form");
        pack();
    }

}
